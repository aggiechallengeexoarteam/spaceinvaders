﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;
using System;

namespace AggieChallenge.AR
{
    public class EnemySpawner : Singleton<EnemySpawner>
    {
        public float enemySpawnDistance;
        public GameObject currentEnemy { get; private set; }

        private GameObject enemyPrefab;

        void Start()
        {
            enemyPrefab = AssetChooser.Instance.enemyAsset.prefab;
            SpawnEnemy();
        }

        public void SpawnEnemy() {
            if (WaypointSpawner.Instance.currentWaypoint.tag == "Bonus")
                return;

            Vector3 waypointPosition = WaypointSpawner.Instance.currentWaypoint.transform.position;
            Vector3 spawnPosition = new Vector3(0, 0, enemySpawnDistance);
            Quaternion spawnRotation = Quaternion.AngleAxis(180, Vector3.up) * Quaternion.LookRotation(waypointPosition - spawnPosition, Vector3.up);
            currentEnemy = Instantiate(enemyPrefab, spawnPosition, spawnRotation);
        }

        public void DespawnEnemy()
        {
            if (currentEnemy)
            {
                Timer.Instance.stopTimer();
                Destroy(currentEnemy); 
            }
        }
    } 
}
﻿using UnityEngine;
using HoloToolkit.Unity;
using UnityEngine.SceneManagement;

namespace AggieChallenge.AR
{
    public class MouseClick : Singleton<MouseClick>
    {
        private GameObject TrackingObject;
        private GameObject TrackingObjectInstance;

        void Start()
        {
            TrackingObject = AssetChooser.Instance.handAsset.prefab;
            TrackingObjectInstance = null;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.V))
            {
                if (TrackingObjectInstance == null)
                {
                    TrackingObjectInstance = Instantiate(TrackingObject);
                    TrackingObjectInstance.transform.position = Camera.main.transform.position;
                }

                TouchWaypoint();
            }
            else if (Input.GetKey(KeyCode.Backspace))
                SceneManager.LoadScene("Scenes/Report");
        }

        private void TouchWaypoint()
        {
            if(WaypointSpawner.Instance.currentWaypoint)
            {
                Vector3 waypointPosition = WaypointSpawner.Instance.currentWaypoint.transform.position;
                TrackingObjectInstance.transform.position = Vector3.MoveTowards(TrackingObjectInstance.transform.position, waypointPosition, Time.deltaTime);
            }
            else
            {
                Destroy(TrackingObjectInstance);
            }        
        }
    } 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;

public class AssetChooser : Singleton<AssetChooser> {
    [Serializable]
    public class AssetBundle
    {
        public GameObject prefab;
        public AudioClip clip;
    }

    public AssetBundle[] waypointAssets;
    public AssetBundle enemyAsset;
    public AssetBundle handAsset;

    public AudioClip findClip(GameObject asset)
    {
        foreach (AssetBundle bundle in waypointAssets)
        {
            if (asset.tag == bundle.prefab.tag)
            {
                return bundle.clip;
            }
        }

        return null;
    }
}

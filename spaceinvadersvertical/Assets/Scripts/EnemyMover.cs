﻿using UnityEngine;
using HoloToolkit.Unity;
using System.Collections;

namespace AggieChallenge.AR
{
    public class EnemyMover : MonoBehaviour
    {
        public float speed; // meters per second divided by 50 (fixed update refresh rate)
        private Vector3 waypointPos;
        private Vector3 target;

        void Start()
        {
            waypointPos = WaypointSpawner.Instance.currentWaypoint.transform.position;
            /* target = waypointPos - new Vector3(0, waypointPos.y, -2*waypointPos.y);*/ // initially moves to position 45 deg from waypoint
            target = waypointPos;
            
            float time = (Vector3.Magnitude(target-transform.position)+Vector3.Magnitude(waypointPos-target))/(52*speed); // seconds it will take enemy to reach waypoint (plus a little bit)
            Timer.Instance.startTimer(time);
        }

        void FixedUpdate()
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed);
            if(transform.position == target)
            {
                target = waypointPos;
            }              
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorSpeed : MonoBehaviour {

    private Animator anim;
	
	void Start () {
        anim = GetComponent<Animator>();
        anim.speed = 5;
	}
}
